/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

/**
 *
 * @author Junior
 */
public class Veiculo {
    
    private int id;
    private String tipo;
    private String marca;
    private String modelo;
    private String placa;
    private int ano;
    

    public Veiculo() {
    }

    public Veiculo(int id, String tipo, String marca, String modelo, String placa, int ano) {
        this.id = id;
        this.tipo = tipo;
        this.marca = marca;
        this.modelo = modelo;
        this.placa = placa;
        this.ano = ano;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    @Override
    public String toString() {
        return "Veiculo{" + "id= " + this.id + ", tipo= " + this.tipo + ", marca= " + this.marca + ", modelo= " + this.modelo + ", placa= " + this.placa + ", ano= " + this.ano + "}";
    }
    
    
}
