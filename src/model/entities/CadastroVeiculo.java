package model.entities;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import javax.swing.JOptionPane;


public class CadastroVeiculo {

    public void verificarPastaArquivo() throws IOException {

        File pastaVeiculo = new File("src/");
        boolean checarPastaVeiculo = pastaVeiculo.exists();
        if (checarPastaVeiculo == false) {

            File novaPastaVeiculo = new File("src/Arquivos/");
            novaPastaVeiculo.mkdir();

            File arquivoTxt = new File("src/Arquivos/arquivo.txt");
            boolean checarArquivoVeiculo = arquivoTxt.exists();

            if (checarArquivoVeiculo == false) {

                arquivoTxt.createNewFile();
                JOptionPane.showMessageDialog(null, "Arquivo criado com sucesso");
            }

        }

    }

    public void veiculosCadastrados() throws FileNotFoundException, IOException {
        InputStream leitorByte = null;
        InputStreamReader leitorCaracter = null;
        BufferedReader leitorString = null;

        try {

            leitorByte = new FileInputStream("src/Arquivos/arquivo.txt");
            leitorCaracter = new InputStreamReader(leitorByte);
            leitorString = new BufferedReader(leitorCaracter);

            String linha = "";
            String frasecompleta = "";
            while (linha != null) {
                linha = leitorString.readLine();
                if (linha != null) {
                    frasecompleta = frasecompleta + "\n " + linha;
                }
            }
            JOptionPane.showMessageDialog(null, "" + frasecompleta);
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {

            leitorByte.close();
            leitorCaracter.close();
            leitorString.close();
        }

    }

    public void escreverArquivo(Veiculo veiculo) throws FileNotFoundException, IOException {
        OutputStream escritorByte = null;
        OutputStreamWriter escritorCaracter = null;
        BufferedWriter escritorFrase = null;

        escritorByte = new FileOutputStream("src/Arquivos/arquivo.txt", true);
        escritorCaracter = new OutputStreamWriter(escritorByte);
        escritorFrase = new BufferedWriter(escritorCaracter);

        
       
        escritorFrase.write(veiculo.toString());
        escritorFrase.newLine();
        escritorFrase.flush();
        
        escritorFrase.close();
        escritorCaracter.close();
        escritorByte.close();
    }

}
