package model.entities;

public class Cliente extends Pessoa {

    
    public Cliente() {
    }

    public Cliente(int id, String nome, String rua, String bairro, String cidade, String estado, String pais, int numero) {
        super(id, nome, rua, bairro, cidade, estado, pais, numero);
    }
}
