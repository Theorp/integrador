/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

import java.util.Date;

/**
 *
 * @author Junior
 */
public class Produto {
    
    private Integer idLoc;
    private Cliente clienteRemetente;
    private Cliente clienteDestinatario;
    private Date dataDeposito;
    private Double peso;

    public Produto() {
    }

    public Produto(Integer idLoc, Cliente clienteRemetente, Cliente clienteDestinatario, Date dataDeposito, Double peso) {
        this.idLoc = idLoc;
        this.clienteRemetente = clienteRemetente;
        this.clienteDestinatario = clienteDestinatario;
        this.dataDeposito = dataDeposito;
        this.peso = peso;
    }

    public Produto(int idLoc, String clienteRemetente, String clienteDestinatario, String dataDeposito, Float peso) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Produto(int idLoc, String clienteRemetente, String clienteDestinatario, String dataDeposito, Double peso) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Integer getIdLoc() {
        return idLoc;
    }

    public void setIdLoc(Integer idLoc) {
        this.idLoc = idLoc;
    }

    public Cliente getClienteRemetente() {
        return clienteRemetente;
    }

    public void setClienteRemetente(Cliente clienteRemetente) {
        this.clienteRemetente = clienteRemetente;
    }

    public Cliente getClienteDestinatario() {
        return clienteDestinatario;
    }

    public void setClienteDestinatario(Cliente clienteDestinatario) {
        this.clienteDestinatario = clienteDestinatario;
    }

    public Date getDataDeposito() {
        return dataDeposito;
    }

    public void setDataDeposito(Date dataDeposito) {
        this.dataDeposito = dataDeposito;
    }

    public Double getPeso() {
        return peso;
    }

    public void setPeso(Double peso) {
        this.peso = peso;
    }
    
    public String imprimir(){
        return "Localizador: "     + idLoc               + 
                "\nRemetente: "    + clienteRemetente    +
                "\nDestinatário: " + clienteDestinatario + 
                "\nData Postagem: "+ dataDeposito        +
                "\nPeso: "         + peso                ;
        
    }
}
