/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entities;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Junior
 */
public class ClienteApp {
    
    List<Cliente> list = new ArrayList();
    
    public void incluir(Cliente cliente) throws IOException{
        list.add(cliente);
        String path = "C:\\Users\\jsmj9524\\Documents\\Unisul\\Transportadora\\test\\Arquivos\\Cliente.txt";
        /*String[] linhas = new String[]{cliente.getNome(), cliente.getEndereco().getRua(), "HELLO    "};
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path, true))){
              for(String linha : linhas){
                  bw.write(linha);
                  bw.newLine();
              }
        } catch (IOException e){
            e.printStackTrace();
        }*/
        OutputStream escritorByte = null;
        OutputStreamWriter escritorCaracter = null;
        BufferedWriter escritorFrase = null;

        escritorByte = new java.io.FileOutputStream(path, true);
        escritorCaracter = new OutputStreamWriter(escritorByte);
        escritorFrase = new BufferedWriter(escritorCaracter);

        
       
        escritorFrase.write(cliente.toString());
        escritorFrase.newLine();
        escritorFrase.flush();
        
        escritorFrase.close();
        escritorCaracter.close();
        escritorByte.close();
    }
    
    public Cliente consultar(String nome){
        for(Cliente cliente :list){
            if(cliente.getNome().equals(nome))
                return cliente;
        }
        return null;
    }
    
    public void exibir(Cliente cliente){
        JOptionPane.showMessageDialog(null, cliente);
    }
}
